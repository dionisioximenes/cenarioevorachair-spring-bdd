package Com.test.springbdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

public class org_artigossubmetidos {


	    @Given("I am an organizer of the conference")
	    public void iAmAnOrganizerOfConference() {
	        // Logic to verify that the user is an organizer of the conference
	    }

	    @When("I access the evoraChair system")
	    public void accessEvoraChairSystem() {
	        // Logic to simulate accessing the evoraChair system
	    }

	    @And("I navigate to the submitted articles section")
	    public void navigateToSubmittedArticlesSection() {
	        // Logic to simulate navigating to the submitted articles section
	    }

	    @And("I select an article")
	    public void selectArticle() {
	        // Logic to simulate selecting an article
	    }

	    @And("I assign three reviewers to the selected article")
	    public void assignReviewersToSelectedArticle() {
	        // Logic to assign three reviewers to the selected article
	    }

	    @Then("the reviewers are successfully assigned to the article")
	    public void reviewersSuccessfullyAssigned() {
	        // Logic to verify that the reviewers are successfully assigned to the article
	    }
	}

