package Com.test.springbdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

public class org_alterarrevisores {

	    @Given("I am an organizer of the conference")
	    public void iAmAnOrganizerOfConference() {
	        // Logic to verify that the user is an organizer of the conference
	    }

	    @When("I access the evoraChair system")
	    public void accessEvoraChairSystem() {
	        // Logic to simulate accessing the evoraChair system
	    }

	    @And("I navigate to the submitted articles section")
	    public void navigateToSubmittedArticlesSection() {
	        // Logic to simulate navigating to the submitted articles section
	    }

	    @And("I select an article")
	    public void selectArticle() {
	        // Logic to simulate selecting an article
	    }

	    @And("I modify the list of assigned reviewers for the selected article")
	    public void modifyListOfAssignedReviewersForSelectedArticle() {
	        // Logic to modify the list of assigned reviewers for the selected article
	    }

	    @Then("the list of assigned reviewers is successfully updated for the article")
	    public void assignedReviewersListSuccessfullyUpdated() {
	        // Logic to verify that the list of assigned reviewers is successfully updated for the article
	    }
}

