package com.test.springbdd;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class av_listarartigosavaliacaoController {

    @GetMapping("/avaliacao/artigos")
    public String listarArtigosAvaliacao(@RequestParam("name") String name) {
        return "Lista Avaliação: " + name;
    }
}
