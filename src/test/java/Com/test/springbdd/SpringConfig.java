package Com.test.springbdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", // Localização dos arquivos de feature
		glue = "com.test.springbdd", // Pacote base para encontrar as classes de passos
		plugin = { "pretty", "html:target/cucumber-reports" } // Relatório de saída
)
public class SpringConfig {
	// Classe vazia para executar os testes do Cucumber com o JUnit
}