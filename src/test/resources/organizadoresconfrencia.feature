Feature: Organizadoresconferencia

  Scenario: Define important conference dates
    Given I am an organizer of the conference
    When I access the evoraChair system
    And I navigate to the conference configuration section
    And I define the important dates for the conference, including submission deadlines, evaluation submission deadline, acceptance notification, and conference date
    Then the dates are successfully set and can be modified