Feature: av_submeteravaliacao

  Scenario: Submit Feedback
    Given I am an evaluator
    When I access the evoraChair system
    And I navigate to the assigned articles section
    And I select an article to evaluate
    And I submit my evaluation for the selected article
    Then my evaluation, including comments and a rating from 0 to 5, is successfully submitted for the article
