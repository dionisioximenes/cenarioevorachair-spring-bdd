package com.test.springbdd;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrganizadoresconferenciaController {

    @GetMapping("/organizadores/conferencia")
    public String getOrganizadoresConferencia() {


        return "Lista de organizadores da conferência";
    }
}
