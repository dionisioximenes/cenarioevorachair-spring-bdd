Feature: org_artigossubmetidos

  Scenario: Assign reviewers to articles
    Given I am an organizer of the conference
    When I access the evoraChair system
    And I navigate to the submitted articles section
    And I select an article
    And I assign three reviewers to the selected article
    Then the reviewers are successfully assigned to the article