Feature: org_revisarmenos2revisor

  Scenario: Review articles with less than 2 reviews
    Given I am an organizer of the conference
    When I access the evoraChair system
    And the evaluation deadline has passed
    And some articles have received less than two evaluations
    And I navigate to the article evaluations section
    And I update the list of assigned reviewers for the articles with less than two evaluations
    Then the articles are scheduled for re-evaluation with an updated list of reviewers
