Feature: org_alterarrevisores

  Scenario: Change list of reviewers assigned to an article
    Given I am an organizer of the conference
    When I access the evoraChair system
    And I navigate to the submitted articles section
    And I select an article
    And I modify the list of assigned reviewers for the selected article
    Then the list of assigned reviewers is successfully updated for the article