Feature: artigossubmetidos

  Scenario: List submitted articles
    Given I am an organizer of the conference
    When I access the evoraChair system
    And I navigate to the submitted articles section
    Then I should be able to view all the submitted articles