Feature: autorconsultar

  Scenario: Check the status of a submission
    Given I am an author
    When I access the evoraChair system
    And I navigate to the article submission section
    Then I should be able to view the status of my submitted articles