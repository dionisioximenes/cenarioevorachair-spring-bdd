Feature: av_listarartigosavaliacao

  Scenario: List/consult articles for review
    Given I am an evaluator
    When I access the evoraChair system
    And I navigate to the assigned articles section
    Then I should be able to list and view all the articles assigned to me for evaluation
