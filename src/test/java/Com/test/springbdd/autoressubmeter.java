package Com.test.springbdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

public class autoressubmeter {
	
	
	    @Given("I am the author")
	    public void iAmTheAuthor() {
	        // Logic to check if the user is an author
	    }

	    @When("accessing the uevorachair system")
	    public void accessingTheUevoraChairSystem() {
	        // Logic to access the uevorachair system
	    }

	    @And("navigate to the submit article page")
	    public void navigateToSubmitArticlePage() {
	        // Logic to navigate to the submit article page
	    }

	    @And("choose and submit article in PDF file")
	    public void chooseAndSubmitArticleInPDFFile() {
	        // Logic to choose and submit the article in PDF format
	    }

	    @And("provide information for reviewers such as name, email, and institution")
	    public void provideInformationForReviewers() {
	        // Logic to provide information for reviewers, such as name, email, and institution
	    }

	    @Then("submit successfully")
	    public void submitSuccessfully() {
	        // Logic to verify that the article submission was successful
	    }
}

