package Com.test.springbdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

public class artigossubmetidos {

	    @Given("I am an organizer of the conference")
	    public void iAmAnOrganizerOfConference() {
	        // Logic to check if the user is an organizer of the conference
	    }

	    @When("I access the evoraChair system")
	    public void accessEvoraChairSystem() {
	        // Logic to access the evoraChair system
	    }

	    @And("I navigate to the submitted articles section")
	    public void navigateToSubmittedArticlesSection() {
	        // Logic to navigate to the submitted articles section
	    }

	    @Then("I should be able to view all the submitted articles")
	    public void viewAllSubmittedArticles() {
	        // Logic to retrieve and display all the submitted articles
	    }
}

