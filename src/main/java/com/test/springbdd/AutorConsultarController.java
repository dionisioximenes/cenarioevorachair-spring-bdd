package com.test.springbdd;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class AutorConsultarController {

    @GetMapping("/autor")
    public String consultarAutor(@RequestParam("name") String name) {

        return "Informação sobre autor: " + name;
    }
}
