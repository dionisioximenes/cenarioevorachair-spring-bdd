Feature: org_definirclassificacaofinal

  Scenario: Define final ranking of articles
    Given I am an organizer of the conference
    When I access the evoraChair system
    And the evaluation deadline has passed
    And all articles have received three evaluations
    And I navigate to the article evaluations section
    And I define the final classification for each article
    Then the final classification is successfully set for all articles with three evaluations
