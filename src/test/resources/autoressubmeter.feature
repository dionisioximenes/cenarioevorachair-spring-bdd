Feature: Autoressubmeter

  Scenario: Submit Articles
    Given I am the author
    When accessing the uevorachair system
    And navigate to the submit article page
    And choose and submit article in PDF file
    And provide information for reviewers such as name, email, and institution
    Then submit successfully