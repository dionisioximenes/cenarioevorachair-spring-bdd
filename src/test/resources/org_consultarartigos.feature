Feature: org_consultarartigos

  Scenario: See article reviews
    Given I am an organizer of the conference
    When I access the evoraChair system
    And I navigate to the article evaluations section
    Then I should be able to view all the evaluations for each article